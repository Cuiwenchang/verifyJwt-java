import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import net.iharder.Base64;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

/**
 * @Auther: zhlong.wang
 * @Date: 4/28/18 15:56
 * @Description:
 */
public class Test {

    public static void main(String[] args) {
        RSAPublicKey publicKey = null;
        //Token 示例
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNvbnN1bWVyQGsyZGF0YS5jb20uY24iLCJleHAiOjE1MjQ4ODUwNDIsImlkIjoiY29uc3VtZXJAazJkYXRhLmNvbS5jbiIsImlzcyI6ImlhbSIsIm9yaWdfaWF0IjoxNTI0ODg0NDQyLCJ1c2VyX2lkIjoidXNyLTY5NjE2ZGEwOTZmZDgxZWJiZWM1OTRhZGFiNTM3MWFhNzFmNzY3IiwidXNlcl90eXBlIjoiY29uc3VtZXIifQ.V4b-oN14Lm_gnrE_bccLVmUq6hqfbSocd7bQfw30mhvy1cSsA3M0-12s3MualRuBD42_MJLSyPb_StUGlp-hjw";

        //加载pub.key公钥文件
        try {
            publicKey = loadPublicKeyByStr();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //创建RS256算法实例
        Algorithm algorithm = Algorithm.RSA256(publicKey, null);

        //创建Token的验证实例
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("iam") //设定签发者是绿能平台iam
                .build(); //Reusable verifier instance

        try {
            //验证Token
            DecodedJWT jwt = verifier.verify(token);
        } catch (JWTVerificationException exception) {
            //验证Token，报异常，比如签发者异常、超时异常
            System.out.println(exception.toString());
            //Invalid Signing configuration / Couldn't convert Claims.
        } finally {
            //解析Payload
            try {
                DecodedJWT jwt = JWT.decode(token);
                System.out.printf("iss:      %s\n", jwt.getClaim("iss").asString());
                System.out.printf("orig_iat: %d\n", jwt.getClaim("orig_iat").asInt());
                System.out.printf("exp:      %s\n", jwt.getClaim("exp").asInt());
                System.out.printf("id:       %s\n", jwt.getClaim("id").asString());
                System.out.printf("user_id:  %s\n", jwt.getClaim("user_id").asString());
                System.out.printf("user_type:%s\n", jwt.getClaim("user_type").asString());
                System.out.printf("email:    %s\n", jwt.getClaim("email").asString());

            } catch (JWTDecodeException exception) {
                //Invalid token
            }
        }
    }

    public static RSAPublicKey loadPublicKeyByStr()
            throws Exception {
        //加载pub.key文件

        File file = new File("src/main/java/pub.key");
        InputStreamReader reader = new InputStreamReader(new FileInputStream(file));
        String temp = IOUtils.toString(reader);
        String publicKeyPEM = temp.replace("-----BEGIN PUBLIC KEY-----\n", "");
        publicKeyPEM = publicKeyPEM.replace("-----END PUBLIC KEY-----", "");

        try {
            byte[] buffer = Base64.decode(publicKeyPEM);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            return (RSAPublicKey) keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("公钥非法");
        } catch (NullPointerException e) {
            throw new Exception("公钥数据为空");
        }
    }
}
